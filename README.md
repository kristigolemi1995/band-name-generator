# Band-name-generator


## Name
Choose a self-explaining name for your project.

## Description
This is a simple project which generates a name band by combining the give answers by the user.


## Usage
You only need to answer to the questions and trying multiple times it's allowed.

## Authors and acknowledgment
This project was rewritten by Kristi Golemi while following 100 Days of Code: The Complete Python Pro Bootcamp.

